var Remarkable = require('remarkable');
var furi = require('../index.js');
var assert = require('assert');
var ut = require("../util.js");

var md = new Remarkable('full');
md.use(furi, {'single': true, 'brackets': true, 'infer_kanji': true});

describe("Both", function () {

    describe("Single tests", function () {
        var test1 = '本（ほん）';
        it(test1 + " should produce 1 ruby token", function () {
            ut.assert_token_numbers(test1, md, {'ruby': 1});
        });

        var test2 = test1 + "[here](http:example.com) hi";
        it(test2 + " should produce 1 ruby and 1 link", function () {
            ut.assert_token_numbers(test2, md, {'ruby': 1, 'link_open': 1});
        });
    });

    describe("Bracket tests", function() {
        var test1 = '{base}(ruby)';
        it(test1 + " should produce 1 ruby token", function () {
            var tokens = md.parseInline(test1, {});
            var rubies = ut.findruby(tokens);
            assert.equal(rubies.length,1);
        });
        var test2 = '{base}(ruby)[here](http://example.com){base}(ruby)';
        it(test2 + " should produce 2 ruby token 1 link", function () {
            var tokens = md.parseInline(test2, {});
            var rubies = ut.findruby(tokens);
            var links = ut.findtokens(tokens, 'link_open');
            assert.equal(rubies.length,2);
        });
        var test3 = "{base}(ruby){base }( ruby)[here](http://example.com){base}(ruby)\n# more blah blah\n{  base   }(  ru）by )\n\n";
        it("Should produce 4 ruby tokens 1 link", function() {
            var tokens = md.parse(test3, {});
            //assert.equal(ut.findtokens(tokens, 'link_open').length, 1);
            assert.equal(ut.findtokens(tokens, 'ruby').length, 4);
        });

        var test4 ="｛漢字｝（かんじ）"
        it(test4 + " should produce one ruby token", function () {
            var tokens = md.parse(test4,{});
            assert.equal(ut.findruby(tokens).length, 1);
        });

    });

    describe("mixed usage", function () {
        var test1 = "本（ほん）{連合国軍占領下}(れんごうこくぐんせんりょうか)";
        it(test1 + "should produce 2 ruby tokens", function () {
            ut.assert_token_numbers(test1, md, {'ruby': 2});
        });
    });
    
    describe("unrelated text", function() {
      var test_text = "(i**){}";

      // NOTE: This requires 'full'.
      it("random placement of '++' shouldn't break the parser", function() {
          md.render(test_text);
      });
    });
    
});
