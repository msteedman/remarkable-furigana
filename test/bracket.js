var Remarkable = require('remarkable');
var furi = require('../index.js');
var assert = require('assert');
var ut = require("../util.js");

var md = new Remarkable();
md.use(furi);

describe('Brackets', function() {

    var test1 = '{base}(ruby)';
    it(test1 + " should produce 1 ruby token", function () {
        var tokens = md.parseInline(test1, {});
        var rubies = ut.findruby(tokens);
        assert.equal(rubies.length,1);
    });
    var test2 = '{base}(ruby)[here](http://example.com){base}(ruby)';
    it(test2 + " should produce 2 ruby token 1 link", function () {
        var tokens = md.parseInline(test2, {});
        var rubies = ut.findruby(tokens);
        var links = ut.findtokens(tokens, 'link_open');
        assert.equal(rubies.length,2);
    });
    var test3 = "{base}(ruby){base }( ruby)[here](http://example.com){base}(ruby)\n# more blah blah\n{  base   }(  ru）by )\n\n";
    it("Should produce 4 ruby tokens 1 link", function() {
        var tokens = md.parse(test3, {});
        //assert.equal(ut.findtokens(tokens, 'link_open').length, 1);
        assert.equal(ut.findtokens(tokens, 'ruby').length, 4);
    });

    var test4 ="｛漢字｝（かんじ）"
    it(test4 + " should produce one ruby token", function () {
        var tokens = md.parse(test4,{});
        assert.equal(ut.findruby(tokens).length, 1);
    });

    var test5 ="{nice}(nice)"
    it(test5 + " should produce one ruby token", function () {
        ut.assert_token_numbers(test5, md, {'ruby': 1});
    });
    var test6 = '**{base}(ruby)**';
    it(test6 + " should produce 1 ruby token", function () {
        var tokens = md.parseInline(test6, {});
        var rubies = ut.findruby(tokens);
        assert.equal(rubies.length,1);
    });
    var test7 = '{**base**}(ruby)';
    it(test7 + " should produce 1 ruby token", function () {
        var tokens = md.parseInline(test7, {});
        var rubies = ut.findruby(tokens);
        assert.equal(rubies.length,1);
    });
    var test8 = '(i**){}';
    it("random placement of inline terminator doesn't break parser", function() {
      assert.equal(md.render(test8), "<p>(i**){}</p>\n");
    });
    var test9 = 'foo\n(i**){}';
    it("newline shouldn't break the parser", function() {
      assert.equal(md.render(test9), "<p>foo\n(i**){}</p>\n");
    });
    var test10 = '気が{利}(き)く';
    it(test10 + " should produce one ruby token", function() {
      assert.equal(
        md.render(test10),
        "<p>気が<ruby>利<rt>き</rt></ruby>く</p>\n"
      )
    });
    var test11 = '気が利 (き)く';
    it(test11 + " should not produce any ruby", function() {

      assert.equal(md.render(test11), "<p>気が利 (き)く</p>\n");
    });
});
    
