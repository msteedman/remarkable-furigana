var assert = require('assert');

function filter(p, xs) {
    var ret = [];
    var j = 0;
    for(i = 0; i < xs.length; i++) {
        if (p(xs[i])) {
            ret[j++] = xs[i];
        }
    }
    return ret;
}

function cdr(xs) {
    return xs.slice(1);
}

function findtokens(tokens, type) {
    function letrec(tokens, res) {
        if (!tokens || tokens.length === 0) {
            return res;
        } else if(tokens[0]['type'] == 'inline') {
            var first = letrec(tokens[0]['children'], res);
            var rest = letrec(cdr(tokens), first);
            return rest;
        } else if (tokens[0]['type'] == type) {
            return letrec(cdr(tokens), ([tokens[0]]).concat(res));
        } else {
            return letrec(cdr(tokens), res);
        }
    }
    tokens = tokens || [];
    return letrec(tokens, []);
}

function findruby(tokens) {
    return findtokens(tokens, 'ruby');
}

function assert_token_numbers(test, md, assertions) {
    var tokens = md.parseInline(test, {});
    for (var tagname in assertions) {
        var tags = findtokens(tokens, tagname);
        assert.equal(tags.length, assertions[tagname]);
    }
}

exports.assert_token_numbers = assert_token_numbers;
exports.filter = filter;
exports.findruby = findruby;
exports.findtokens = findtokens;
